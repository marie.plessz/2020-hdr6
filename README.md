Ce projet est hébergé ici : https://al-dev.versailles-grignon.inra.fr/cmh/2020-hdr6 


Il correspond au chapitre 6 de mon Habilitation à diriger des recherches : 

Marie Plessz. _La Dynamique sociale des pratiques : stratification sociale, changement social et consommation alimentaire. Sociologie_. École des hautes Études en Sciences sociales (EHESS), 2021. ⟨[tel-03436087](https://hal.inrae.fr/tel-03436087)⟩
